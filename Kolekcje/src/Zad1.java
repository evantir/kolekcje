import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zad1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// klucz, warto�� , "witam"=>"hello"
		Map<String,String> dictionary = new HashMap<>();
		dictionary.put("witam", "hello");
		dictionary.put("pies", "dog");
		dictionary.put("kot", "cat");
		dictionary.put("pensja","salary");
		System.out.println("Podaj wyraz po Polsku");
		Scanner scanner = new Scanner(System.in);
		String polishWord = scanner.next();
		if ( dictionary.containsKey(polishWord)   ){
			System.out.println("S�owo po angielsku to "+dictionary.get(polishWord));
		}
		else{
			System.out.println("Nie znaleziono s�owa w s�owniku");
		}
		
	}

}